import numpy as np
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.widgets import Button, Slider
from matplotlib import animation
import datetime
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
import math

# Create global variables
global n_speed, nw_speed, se_speed, sw_speed
global n_direction, nw_direction, se_direction, sw_direction
global n_gen_dir, nw_gen_dir, se_gen_dir, sw_gen_dir
global qplots
global anim_running, is_manual


def update_quiver(in_val, vector_color, gen_dir_types, slider_n):
    """
    Uppdates the quiver plot to show the wind from the past 60 minutes and the current wind
    :param in_val: index value of the current wind reading
    """
    # declare global variables
    global n_speed, nw_speed, se_speed, sw_speed
    global n_direction, nw_direction, se_direction, sw_direction
    global n_gen_dir, nw_gen_dir, se_gen_dir, sw_gen_dir
    global qplot
    global anim_running, is_manual
    # Prevent animation from starting without the button being clicked
    if not anim_running and not is_manual:
        return qplots
    # Clear the plots
    qplots[0].clear()
    qplots[1].clear()
    qplots[2].clear()
    qplots[3].clear()
    # plot previous hours wind speeds and directions
    if in_val <= 60:
        min_index = 0
    else:
        min_index = in_val - 60
    # Update slider value
    if slider_n.val != in_val:
        slider_n.set_val(in_val)
    u = n_speed[min_index:in_val] / n_speed.max() * np.sin(np.deg2rad(n_direction[min_index:in_val]))
    v = n_speed[min_index:in_val] / n_speed.max() * np.cos(np.deg2rad(n_direction[min_index:in_val]))
    qplots[0].quiver(np.zeros(len(u)), np.zeros(len(v)), u, v,
                  color='gray',
                  angles='uv', scale=2)

    u = nw_speed[min_index:in_val] / nw_speed.max() * np.sin(np.deg2rad(nw_direction[min_index:in_val]))
    v = nw_speed[min_index:in_val] / nw_speed.max() * np.cos(np.deg2rad(nw_direction[min_index:in_val]))
    qplots[1].quiver(np.zeros(len(u)), np.zeros(len(v)), u, v,
                  color='gray',
                  angles='uv', scale=2)

    u = se_speed[min_index:in_val] / se_speed.max() * np.sin(np.deg2rad(se_direction[min_index:in_val]))
    v = se_speed[min_index:in_val] / se_speed.max() * np.cos(np.deg2rad(se_direction[min_index:in_val]))
    qplots[2].quiver(np.zeros(len(u)), np.zeros(len(v)), u, v,
                  color='gray',
                  angles='uv', scale=2)

    u = sw_speed[min_index:in_val] / sw_speed.max() * np.sin(np.deg2rad(sw_direction[min_index:in_val]))
    v = sw_speed[min_index:in_val] / sw_speed.max() * np.cos(np.deg2rad(sw_direction[min_index:in_val]))
    qplots[3].quiver(np.zeros(len(u)), np.zeros(len(v)), u, v,
                  color='gray',
                  angles='uv', scale=2)

    # Update current time plot
    u = n_speed[in_val] / n_speed.max() * np.sin(np.deg2rad(n_direction[in_val]))
    v = n_speed[in_val] / n_speed.max() * np.cos(np.deg2rad(n_direction[in_val]))
    qplots[0].quiver(0, 0, u, v,
                  color=vector_color[gen_dir_types.index(n_gen_dir[in_val])],
                  angles='uv', scale=2, width=0.015)
    qplots[0].set_rmax(n_speed.max())

    u = nw_speed[in_val] / nw_speed.max() * np.sin(np.deg2rad(nw_direction[in_val]))
    v = nw_speed[in_val] / nw_speed.max() * np.cos(np.deg2rad(nw_direction[in_val]))
    qplots[1].quiver(0, 0, u, v,
                  color=vector_color[gen_dir_types.index(nw_gen_dir[in_val])],
                  angles='uv', scale=2, width=0.015)
    qplots[1].set_rmax(nw_speed.max())

    u = se_speed[in_val] / se_speed.max() * np.sin(np.deg2rad(se_direction[in_val]))
    v = se_speed[in_val] / se_speed.max() * np.cos(np.deg2rad(se_direction[in_val]))
    qplots[2].quiver(0, 0, u, v,
                  color=vector_color[gen_dir_types.index(se_gen_dir[in_val])],
                  angles='uv', scale=2, width=0.015)
    qplots[2].set_rmax(se_speed.max())

    u = sw_speed[in_val] / sw_speed.max() * np.sin(np.deg2rad(sw_direction[in_val]))
    v = sw_speed[in_val] / sw_speed.max() * np.cos(np.deg2rad(sw_direction[in_val]))
    qplots[3].quiver(0, 0, u, v,
                  color=vector_color[gen_dir_types.index(sw_gen_dir[in_val])],
                  angles='uv', scale=2, width=0.015)
    qplots[3].set_rmax(sw_speed.max())

    # Put title back into subplots
    qplots[0].title.set_text('North Sensor')
    qplots[1].title.set_text('Northwest Sensor')
    qplots[2].title.set_text('Southeast Sensor')
    qplots[3].title.set_text('Southwest Sensor')
    return qplots


def plot_quiver(laken_meas, lakenw_meas, lakese_meas, lakesw_meas, vector_color, gen_dir_types, sensor_colors):
    global n_speed, nw_speed, se_speed, sw_speed
    global n_direction, nw_direction, se_direction, sw_direction
    global n_gen_dir, nw_gen_dir, se_gen_dir, sw_gen_dir
    global qplots
    global anim_running, is_manual
    # Initialize numpy arrays for wind speed

    n_speed = np.array(laken_meas[6])
    nw_speed = np.array(lakenw_meas[6])
    se_speed = np.array(lakese_meas[6])
    sw_speed = np.array(lakesw_meas[6])
    # Initialize numpy arrays for wind direction
    n_direction = np.array(laken_meas[8])
    nw_direction = np.array(lakenw_meas[8])
    se_direction = np.array(lakese_meas[8])
    sw_direction = np.array(lakesw_meas[8])
    # Initialize numpy arrays for general wind direciton
    n_gen_dir = np.array(laken_meas[9])
    nw_gen_dir = np.array(lakenw_meas[9])
    se_gen_dir = np.array(lakese_meas[9])
    sw_gen_dir = np.array(lakesw_meas[9])


    # read map image and create figure with subplots, create hide
    img = plt.imread("map.png")
    hide_fig = plt.figure(figsize=(12, 7))
    fig = plt.figure(figsize=(12, 7))
    map_gs = fig.add_gridspec(2, 3, top=0.85, left=0.025, right=0.9)
    hide_gs = hide_fig.add_gridspec(1, 2, top=0.85, left=0.025, right=0.86, wspace=0.095)
    map = fig.add_subplot(map_gs[::, 0])
    map.imshow(img)
    # Create plot area with four plots
    qplots = []
    qplots.append(fig.add_subplot(map_gs[0, 1], projection='polar'))
    qplots[0].set_theta_direction(-1)
    qplots[0].set_theta_zero_location("N")
    qplots.append(fig.add_subplot(map_gs[0, 2], projection='polar'))
    qplots[1].set_theta_direction(-1)
    qplots[1].set_theta_zero_location("N")
    qplots.append(fig.add_subplot(map_gs[1, 1], projection='polar'))
    qplots[2].set_theta_direction(-1)
    qplots[2].set_theta_zero_location("N")
    qplots.append(fig.add_subplot(map_gs[1, 2], projection='polar'))
    qplots[3].set_theta_direction(-1)
    qplots[3].set_theta_zero_location("N")
    # Specify sensor locations and labels
    x = [643, 558, 545, 314]
    y = [204, 296, 836, 712]
    labels = ['LakeN Sensor', 'LakeNW Sensor', 'LakeSW Sensor', 'LakeSE Sensor']
    # Create array of selected sensors
    selected_sensors = [False, False, False, False]
    # Function for when wind sensor button is clicked.
    def clicked(event, index):
        """Hide or Show sensor plots
        index: corresponding sensor index
        leg: figure legend object
        return: leg
        """
        if not qplots[index].get_visible():
            change = True
        else:
            change = False
        # Change color of selected button and other buttons
        for i in range(0, 4, 1):
            if i < 2:
                orig_pos = i + 1
            else:
                orig_pos = i + 2
            if index == i:
                if buttons[i].color == 'white':
                    buttons[i].color = 'gray'
                    buttons[i].label.set_color('black')
                    map.set_position(hide_gs[0].get_position(fig))
                    qplots[i].set_position(hide_gs[1].get_position(fig))
                    qplots[i].set_visible(True)
                else:
                    buttons[i].color = 'white'
                    buttons[i].label.set_color(sensor_colors[i])
                    map.set_position(map_gs[::, 0].get_position(fig))
                    qplots[i].set_position(map_gs[orig_pos].get_position(fig))
            else:
                buttons[i].color = 'white'
                buttons[i].label.set_color(sensor_colors[i])
                if change:
                    qplots[i].set_visible(False)
                else:
                    qplots[i].set_visible(not qplots[i].get_visible())
                qplots[i].set_position(map_gs[orig_pos].get_position(fig))
        # Hide or show plots based on if the sensor has been selected.
        # Reset buttons to being unselected if all have been selected
        # Get the current values


    # Plot sensors on the map and annotate with boxes
    map.scatter(x, y, color=sensor_colors)
    map.axis('off')

    # Create Sensor Filter buttons
    # Set position of the buttons
    n_loc = plt.axes([0, 0, 1, 1])
    n_ip = InsetPosition(map.axes, [0.565, 0.75, .29, 0.06])
    n_loc.set_axes_locator(n_ip)
    nw_loc = plt.axes([0, 0, 1, 1])
    nw_ip = InsetPosition(map.axes, [0.49, 0.665, .32, 0.06])
    nw_loc.set_axes_locator(nw_ip)
    se_loc = plt.axes([0, 0, 1, 1])
    se_ip = InsetPosition(map.axes, [0.48, 0.16, .32, 0.06])
    se_loc.set_axes_locator(se_ip)
    sw_loc = plt.axes([0, 0, 1, 1])
    sw_ip = InsetPosition(map.axes, [0.28, 0.28, .32, 0.06])
    sw_loc.set_axes_locator(sw_ip)

    # Create buttons, list of buttons and change font
    nbutton = Button(n_loc, 'LakeN Sensor', color='white')
    nwbutton = Button(nw_loc, 'LakeNW Sensor', color='white')
    sebutton = Button(se_loc, 'LakeSE Sensor', color='white')
    swbutton = Button(sw_loc, 'LakeSW Sensor', color='white')
    buttons = [nbutton, nwbutton, sebutton, swbutton]
    nbutton.label.set_color(sensor_colors[0])
    nbutton.label.set_horizontalalignment('center')
    nbutton.label.set_verticalalignment('center')
    nwbutton.label.set_color(sensor_colors[1])
    sebutton.label.set_color(sensor_colors[2])
    swbutton.label.set_color(sensor_colors[3])

    nbutton.on_clicked(lambda x: clicked(x, 0))
    nwbutton.on_clicked(lambda x: clicked(x, 1))
    sebutton.on_clicked(lambda x: clicked(x, 2))
    swbutton.on_clicked(lambda x: clicked(x, 3))


    # Plot origin
    qplots[0].plot(0, 0, color='black', marker='o', markersize=5)
    qplots[1].plot(0, 0, color='black', marker='o', markersize=5)
    qplots[2].plot(0, 0, color='black', marker='o', markersize=5)
    qplots[3].plot(0, 0, color='black', marker='o', markersize=5)
    # Create lines to make custom legend
    legend_lines = []
    for i in range(0, len(vector_color), 1):
        legend_lines.append(Line2D([0], [0], color=vector_color[i], lw=4))
    # Plot agent's path for each sensor
    # Plot N sensor
    u = n_speed[0] / n_speed.max() * np.sin(np.deg2rad(n_direction[0]))
    v = n_speed[0] / n_speed.max() * np.cos(np.deg2rad(n_direction[0]))
    qplots[0].quiver(0, 0, u, v,
                  color=vector_color[gen_dir_types.index(n_gen_dir[0])],
                  angles='uv', scale=2, width=0.015)
    # Plot NW sensor
    u = nw_speed[i] / nw_speed.max() * np.sin(np.deg2rad(nw_direction[i]))
    v = nw_speed[i] / nw_speed.max() * np.cos(np.deg2rad(nw_direction[i]))
    qplots[1].quiver(0, 0, u, v,color=vector_color[gen_dir_types.index(nw_gen_dir[i])], angles='uv', scale=2, width=0.015)
    # Plot SE sensor
    u = se_speed[i] / se_speed.max() * np.sin(np.deg2rad(se_direction[i]))
    v = se_speed[i] / se_speed.max() * np.cos(np.deg2rad(se_direction[i]))
    qplots[2].quiver(0, 0, u, v, color=vector_color[gen_dir_types.index(se_gen_dir[i])], angles='uv', scale=2, width=0.015)
    # Plot SW sensor
    u = sw_speed[i] / sw_speed.max() * np.sin(np.deg2rad(sw_direction[i]))
    v = sw_speed[i] / sw_speed.max() * np.cos(np.deg2rad(sw_direction[i]))
    qplots[3].quiver(0, 0, u, v, color=vector_color[gen_dir_types.index(sw_gen_dir[i])], angles='uv', scale=2, width=0.015)
    # Plot configuration
    # N Sensor configurations
    qplots[0].set_rmin(0)
    qplots[0].set_rmax(n_speed.max())
    qplots[0].set_thetalim(0, 2 * np.pi)
    qplots[0].set_xticks(np.linspace(0, 2 * np.pi, 4, endpoint=False))
    qplots[0].grid(True)
    qplots[0].set_rlabel_position(45)
    # NW sensor configurations
    qplots[1].set_rmin(0)
    qplots[1].set_rmax(nw_speed.max())
    qplots[1].set_thetalim(0, 2 * np.pi)
    qplots[1].set_xticks(np.linspace(0, 2 * np.pi, 4, endpoint=False))
    qplots[1].grid(True)
    qplots[1].set_rlabel_position(45)
    # SE sensor configurations
    qplots[2].set_rmin(0)
    qplots[2].set_rmax(se_speed.max())
    qplots[2].set_thetalim(0, 2 * np.pi)
    qplots[2].set_xticks(np.linspace(0, 2 * np.pi, 4, endpoint=False))
    qplots[2].grid(True)
    qplots[2].set_rlabel_position(45)
    # SW sensor configurations
    qplots[3].set_rmin(0)
    qplots[3].set_rmax(sw_speed.max())
    qplots[3].set_thetalim(0, 2 * np.pi)
    qplots[3].set_xticks(np.linspace(0, 2 * np.pi, 4, endpoint=False))
    qplots[3].grid(True)
    qplots[3].set_rlabel_position(45)
    # create table list and rtick value list to the maximum wind speed
    rlabels = ['', '2 mph', '4 mph', '6 mph', '8 mph', '10 mph', '12 mph', '14 mph', '16 mph', '18 mph', '19 mph', '20 mph',
               '22 mph']
    n_speeds = list(range(0, (math.ceil(n_speed.max() / 2) * 2) + 2, 2))
    nw_speeds = list(range(0, (math.ceil(nw_speed.max() / 2) * 2) + 2, 2))
    se_speeds = list(range(0, (math.ceil(se_speed.max() / 2) * 2) + 2, 2))
    sw_speeds = list(range(0, (math.ceil(sw_speed.max() / 2) * 2) + 2, 2))
    qplots[0].set_rticks(n_speeds, rlabels[0:(len(n_speeds))])
    qplots[1].set_rticks(nw_speeds, rlabels[0:(len(nw_speeds))])
    qplots[2].set_rticks(se_speeds, rlabels[0:(len(se_speeds))])
    qplots[3].set_rticks(sw_speeds, rlabels[0:(len(sw_speeds))])
    # Initialize that the animation is not running nor manual
    anim_running = False
    is_manual = False
    # Create Slider
    slider_n = Slider(plt.axes([0.2, 0.005, 0.25, 0.05]), 'Time', 0, len(n_speed)-1, valinit=0, valstep=1)
    time = datetime.datetime.strptime(laken_meas[0][0], '%Y-%m-%d %H:%M:%S')
    slider_n.valtext.set_text(datetime.datetime.strftime(time, '%m-%d %H:%M'))

    def onchanged(s_value):
        # Specify is_manual as true
        global is_manual
        is_manual = True
        # Update slider text and call update function
        time = datetime.datetime.strptime(laken_meas[0][s_value], '%Y-%m-%d %H:%M:%S')
        slider_n.valtext.set_text(datetime.datetime.strftime(time, '%m-%d %H:%M'))
        update_quiver(s_value, vector_color, gen_dir_types, slider_n)
        # Sepcify is_manual is false
        is_manual = False
    slider_n.on_changed(onchanged)

    def on_click(event):
        global anim_running
        if anim_running:
            #anim.event_source.stop()
            anim.pause()
            anim_running = False
            button.label.set_text('Start Animation')
        else:
            #anim.event_source.start()
            anim.resume()
            anim_running = True
            button.label.set_text('Stop Animation')

    def gen():
        global anim_running
        i = 0
        while anim_running and i <= len(n_speed)-1:
            if i == len(n_speed)-1:
                i = 0
            elif i != slider_n.val:
                i = slider_n.val
            else:
                i+=1
            yield i
        else:
            yield i

    # Create start and stop animation button
    button_loc = plt.axes([0.65, 0.005, 0.15, 0.05])
    button = Button(button_loc, 'Start Animation')
    button.on_clicked(on_click)

    # Start animation function
    anim = animation.FuncAnimation(fig, update_quiver,
                                   fargs=(vector_color, gen_dir_types, slider_n),
                                   frames=gen, blit=False)



    fig.suptitle('Wind Sensor Direction and Speed', fontweight='bold')
    qplots[0].title.set_text('North Sensor')
    qplots[0].title.set_color(color=sensor_colors[0])
    qplots[1].title.set_text('Northwest Sensor')
    qplots[1].title.set_color(color=sensor_colors[1])
    qplots[2].title.set_text('Southeast Sensor')
    qplots[2].title.set_color(color=sensor_colors[2])
    qplots[3].title.set_text('Southwest Sensor')
    qplots[3].title.set_color(color=sensor_colors[3])
    fig.legend(legend_lines, gen_dir_types, loc='center right')
    plt.subplots_adjust(wspace=0, hspace=.4)
    plt.close(hide_fig)
    plt.show()
