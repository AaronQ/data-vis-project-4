import numpy as np
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.widgets import Button, Slider
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
import datetime

def create_map(vector_color, gen_dir_types, sensor_colors):
    # Create colors for general directions
    # read map image and create figure with subplots
    img = plt.imread("map.png")
    fig = plt.figure(figsize=(10, 7))
    map_gs = fig.add_gridspec(2, 2)
    map = fig.add_subplot(map_gs[::, 0])
    map.imshow(img)
    solar_plt = fig.add_subplot(map_gs[0, 1])
    temp_plt = fig.add_subplot(map_gs[1, 1])
    # Specify sensor locations and labels
    x = [643, 558, 314, 545]
    y = [204, 296, 712, 836]
    labels = ['LakeN Sensor', 'LakeNW Sensor', 'LakeSW Sensor', 'LakeSE Sensor']
    # Create array of selected sensors
    selected_sensors = [False, False, False, False]
    # Function for when wind sensor button is clicked.
    def clicked(event, index):
        """Removes or adds plot lines to change sensor data
        event: button that was clicked on
        index: corresponding sensor index
        """
        # Change the status of the selected sensor
        selected_sensors[index] = not selected_sensors[index]
        # Change color of selected button
        if selected_sensors[index]:
            buttons[index].color = 'gray'
            buttons[index].label.set_color('black')
        else:
            buttons[index].color = 'white'
            buttons[index].label.set_color(vector_color[index])
        # Hide or show curves based on if they have been selected.
        # Reset buttons to being unselected if all have been selected
        if all(selected_sensors):
            for i in range(0, 4, 1):
                buttons[i].color = 'white'
                buttons[i].label.set_color(vector_color[i])
                selected_sensors[i] = False
                solar_lines[i][0].set_visible(True)
                temp_lines[i][0].set_visible(True)
        else:
            for i in range(0, 4, 1):
                if not selected_sensors[i]:
                    solar_lines[i][0].set_visible(False)
                    temp_lines[i][0].set_visible(False)
                else:
                    solar_lines[i][0].set_visible(True)
                    temp_lines[i][0].set_visible(True)
    # Plot sensors on the map and annotate with boxes
    map.scatter(x, y, color=vector_color)
    map.axis('off')

    # Create Sensor Filter buttons
    # Set position of the buttons
    n_loc = plt.axes([0, 0, 1, 1])
    n_ip = InsetPosition(map.axes, [0.565, 0.75, .29, 0.06])
    n_loc.set_axes_locator(n_ip)
    nw_loc = plt.axes([0, 0, 1, 1])
    nw_ip = InsetPosition(map.axes, [0.49, 0.665, .32, 0.06])
    nw_loc.set_axes_locator(nw_ip)
    se_loc = plt.axes([0, 0, 1, 1])
    se_ip = InsetPosition(map.axes, [0.28, 0.28, .32, 0.06])
    se_loc.set_axes_locator(se_ip)
    sw_loc = plt.axes([0, 0, 1, 1])
    sw_ip = InsetPosition(map.axes, [0.48, 0.16, .32, 0.06])
    sw_loc.set_axes_locator(sw_ip)

    # Create buttons, list of buttons and change font
    nbutton = Button(n_loc, 'LakeN Sensor', color='white')
    nwbutton = Button(nw_loc, 'LakeNW Sensor', color='white')
    sebutton = Button(se_loc, 'LakeSE Sensor', color='white')
    swbutton = Button(sw_loc, 'LakeSW Sensor', color='white')
    buttons = [nbutton, nwbutton, sebutton, swbutton]
    nbutton.label.set_color(vector_color[0])
    nbutton.label.set_horizontalalignment('center')
    nbutton.label.set_verticalalignment('center')
    nwbutton.label.set_color(vector_color[1])
    sebutton.label.set_color(vector_color[2])
    swbutton.label.set_color(vector_color[3])

    nbutton.on_clicked(lambda x: clicked(x, 0))
    nwbutton.on_clicked(lambda x: clicked(x, 1))
    sebutton.on_clicked(lambda x: clicked(x, 2))
    swbutton.on_clicked(lambda x: clicked(x, 3))
    #for i, label in enumerate(labels):
        #map.annotate(label, (x[i], y[i]), xytext=(x[i]+30, y[i]+55),
                    #color=vector_color[i], bbox=dict(boxstyle='round', facecolor='white', pad=0.2))
    # Loop through the four sensors to plot the data
    solar_lines = []
    temp_lines = []
    for i in range(0, len(labels), 1):
        # Plot solar radiation over time and temperature over time.
        solar_lines.append(solar_plt.plot(total_data[i][0], total_data[i][12], color=vector_color[i]))
        temp_lines.append(temp_plt.plot(total_data[i][0], total_data[i][4], color=vector_color[i]))
    # Create lines to make custom legend
    legend_lines = []
    for i in range(0, len(vector_color), 1):
        legend_lines.append(Line2D([0], [0], color=vector_color[i], lw=4))


    fig.legend(legend_lines, labels)
    plt.show()
