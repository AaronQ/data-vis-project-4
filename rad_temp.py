from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.widgets import Button, Slider, RadioButtons
import matplotlib.dates as mdates
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
import datetime

def roundTime(dt=None, roundTo=60):
   """Round a datetime object to any time lapse in seconds
   dt : datetime.datetime object, default now.
   roundTo : Closest number of seconds to round to, default 1 minute.
   Author: Thierry Husson 2012 - Use it as you want but don't blame me.
   """
   if dt == None :
       dt = datetime.datetime.now()
   seconds = (dt.replace(tzinfo=None) - dt.min).seconds
   rounding = (seconds+roundTo/2) // roundTo * roundTo
   return dt + datetime.timedelta(0,rounding-seconds,-dt.microsecond)

def shade_temp(laken_meas, lakenw_meas, lakese_meas, lakesw_meas, vector_color, gen_dir_types, sensor_colors):
    # Create colors for general directions
    total_data = [laken_meas, lakenw_meas, lakese_meas, lakesw_meas]
    # read map image and create figure with subplots
    img = plt.imread("map.png")
    fig = plt.figure(figsize=(10, 7))
    map_gs = fig.add_gridspec(2, 2, top=0.95, left=0.025, right=.85)
    map = fig.add_subplot(map_gs[::, 0])
    map.imshow(img)
    solar_plt = fig.add_subplot(map_gs[0, 1])
    temp_plt = fig.add_subplot(map_gs[1, 1])
    # Specify sensor locations and labels
    x = [643, 558, 545, 314]
    y = [204, 296, 836, 712]
    labels = ['LakeN Sensor', 'LakeNW Sensor', 'LakeSW Sensor', 'LakeSE Sensor']
    # Create array of selected sensors
    selected_sensors = [False, False, False, False]
    # Function for when wind sensor button is clicked.
    def clicked(event, index):
        """Removes or adds plot lines to change sensor data
        event: button that was clicked on
        index: corresponding sensor index
        leg: figure legend object
        return: leg
        """
        # Change the status of the selected sensor
        selected_sensors[index] = not selected_sensors[index]
        # Change color of selected button
        if selected_sensors[index]:
            buttons[index].color = 'gray'
            buttons[index].label.set_color('black')
        else:
            buttons[index].color = 'white'
            buttons[index].label.set_color(sensor_colors[index])
        # Hide or show curves based on if they have been selected.
        # Reset buttons to being unselected if all have been selected
        # Change the legend
        leg = fig.legends[0]
        leg.remove()
        temp_leg_lines = legend_lines.copy()
        temp_leg_handles = labels.copy()
        if all(selected_sensors) or not any(selected_sensors):
            for i in range(0, 4, 1):
                buttons[i].color = 'white'
                buttons[i].label.set_color(sensor_colors[i])
                selected_sensors[i] = False
                solar_lines[i][0].set_visible(True)
                temp_lines[i][0].set_visible(True)
        else:
            for i in range(0, 4, 1):
                if not selected_sensors[i]:
                    solar_lines[i][0].set_visible(False)
                    temp_lines[i][0].set_visible(False)
                    temp_leg_lines.remove(legend_lines[i])
                    temp_leg_handles.remove(labels[i])
                else:
                    solar_lines[i][0].set_visible(True)
                    temp_lines[i][0].set_visible(True)
        fig.legend(temp_leg_lines, temp_leg_handles, fontsize='small', loc='upper right', bbox_to_anchor=(0.9975, 0.96))
    # Plot sensors on the map and annotate with boxes
    map.scatter(x, y, color=sensor_colors)
    map.axis('off')

    def time_change(value):
        """
        Changes the axis of both plots to change the time period shown
        :param min_max: Slidervalue
        """
        # Get interval range
        lower = int(value - (last-first)/2)
        upper = int(value + (last-first)/2)
        time_slider.valmax = (len(time) - 1 - ((last - first) / 2))
        time_slider.valmin = ((last - first) / 2)
        for i in range(0, 4, 1):
            solar_lines[i][0].set_xdata(time[lower:upper])
            solar_lines[i][0].set_ydata(total_data[i][12][lower:upper])
            temp_lines[i][0].set_xdata(time[lower:upper])
            temp_lines[i][0].set_ydata(total_data[i][4][lower:upper])
        solar_plt.relim()
        solar_plt.autoscale_view()
        temp_plt.relim()
        temp_plt.autoscale_view()
        time_slider.valtext.set_text(datetime.datetime.strftime(time[lower], '%m-%d %H:%M') + ' to \n' +
                                    datetime.datetime.strftime(time[upper], '%m-%d %H:%M'))


    def int_select(label):
        """
        Changes the size of the interval based on the selected radiobuttons
        :param label: label of the radiobutton
        """
        nonlocal first, last
        if label == '1 hour':
            first = 0
            last = 60
            temp_val = 30
            solar_plt.xaxis.set_major_locator(mdates.MinuteLocator(byminute=[0, 30]))
            temp_plt.xaxis.set_major_locator(mdates.MinuteLocator(byminute=[0, 30]))
        elif label == '6 hours':
            first = 0
            last = 360
            temp_val = 180
            solar_plt.xaxis.set_major_locator(mdates.MinuteLocator(byminute=[0, 30]))
            temp_plt.xaxis.set_major_locator(mdates.MinuteLocator(byminute=[0, 30]))
        elif label == '12 hours':
            first = 0
            last = 720
            temp_val = 360
            solar_plt.xaxis.set_major_locator(mdates.MinuteLocator(byminute=[0, 60]))
            temp_plt.xaxis.set_major_locator(mdates.MinuteLocator(byminute=[0, 60]))
        else:
            first = 0
            last = 1440
            temp_val = 720
            solar_plt.xaxis.set_major_locator(mdates.HourLocator(interval=2))
            temp_plt.xaxis.set_major_locator(mdates.HourLocator(interval=2))
        if all([time_slider.val != 30, time_slider.val != 180, time_slider.val != 360, time_slider.val != 720]):
            temp_val = time_slider.val
        else:
            time_slider.set_val(temp_val)
        time_change(temp_val)


    # Create Sensor Filter buttons
    # Set position of the buttons
    n_loc = plt.axes([0, 0, 1, 1])
    n_ip = InsetPosition(map.axes, [0.565, 0.75, .29, 0.06])
    n_loc.set_axes_locator(n_ip)
    nw_loc = plt.axes([0, 0, 1, 1])
    nw_ip = InsetPosition(map.axes, [0.49, 0.665, .32, 0.06])
    nw_loc.set_axes_locator(nw_ip)
    se_loc = plt.axes([0, 0, 1, 1])
    se_ip = InsetPosition(map.axes, [0.48, 0.16, .32, 0.06])
    se_loc.set_axes_locator(se_ip)
    sw_loc = plt.axes([0, 0, 1, 1])
    sw_ip = InsetPosition(map.axes, [0.28, 0.28, .32, 0.06])
    sw_loc.set_axes_locator(sw_ip)

    # Create buttons, list of buttons and change font
    nbutton = Button(n_loc, 'LakeN Sensor', color='white')
    nwbutton = Button(nw_loc, 'LakeNW Sensor', color='white')
    sebutton = Button(se_loc, 'LakeSE Sensor', color='white')
    swbutton = Button(sw_loc, 'LakeSW Sensor', color='white')
    buttons = [nbutton, nwbutton, sebutton, swbutton]
    nbutton.label.set_color(sensor_colors[0])
    nbutton.label.set_horizontalalignment('center')
    nbutton.label.set_verticalalignment('center')
    nwbutton.label.set_color(sensor_colors[1])
    sebutton.label.set_color(sensor_colors[2])
    swbutton.label.set_color(sensor_colors[3])

    nbutton.on_clicked(lambda x: clicked(x, 0))
    nwbutton.on_clicked(lambda x: clicked(x, 1))
    sebutton.on_clicked(lambda x: clicked(x, 2))
    swbutton.on_clicked(lambda x: clicked(x, 3))


    # Loop through the four sensors to plot the data
    solar_lines = []
    temp_lines = []
    first = 0
    last = 60
    time = [datetime.datetime.strptime(laken_meas[0][i], '%Y-%m-%d %H:%M:%S') for i, value in enumerate(laken_meas[0])]
    for i in range(0, len(labels), 1):
        # Plot solar radiation over time and temperature over time.
        solar_lines.append(solar_plt.plot(time[first:last], total_data[i][12][first:last], color=sensor_colors[i]))
        temp_lines.append(temp_plt.plot(time[first:last], total_data[i][4][first:last], color=sensor_colors[i]))
    plt.subplots_adjust(hspace=0.25)
    # Create lines to make custom legend

    # Create rangeslider for selecting span of plot range

    time_slider = Slider(plt.axes([0.075, 0.05, 0.20, 0.03]), 'Minutes ', 0, len(time) - 1-((last-first)/2),
                         valinit=((last-first)/2), valfmt='%-G', valstep=1)
    time_slider.valtext.set_text(datetime.datetime.strftime(time[first], '%m-%d %H:%M') + ' to \n' +
                                 datetime.datetime.strftime(time[last], '%m-%d %H:%M'))
    time_slider.valtext.set(wrap=True)
    time_slider.on_changed(time_change)

    # Create Radio buttons
    rax = plt.axes([0.025, 0.1, 0.1, 0.15])
    radio_button = RadioButtons(rax, ('1 hour', '6 hours', '12 hours', '24 hours'))
    radio_button.on_clicked(int_select)

    legend_lines = []
    for i in range(0, len(sensor_colors), 1):
        legend_lines.append(Line2D([0], [0], color=sensor_colors[i], lw=4))
    # Set the titles and axis labels of the plots
    solar_plt.set_title('Solar Radiation vs Time')
    solar_plt.set_xlabel('Time (hours)')
    solar_plt.set_ylabel('$Solar Radiation (W/m^2)$')
    temp_plt.set_title('Temperature vs Time')
    temp_plt.set_xlabel('Time (hours)')
    temp_plt.set_ylabel('$Temperature (^oF)$')
    # Modify the plot axis and tick marks
    plt.draw()
    solar_plt.set_xticks(solar_plt.get_xticks())
    solar_plt.set_xticklabels([])
    solar_plt.xaxis.set_major_locator(mdates.MinuteLocator(byminute=[0, 30]))
    temp_plt.set_xticks(temp_plt.get_xticks())
    temp_plt.set_xticklabels(temp_plt.get_xticks(), rotation=45)
    temp_plt.xaxis.set_major_locator(mdates.MinuteLocator(byminute=[0, 30]))
    temp_plt.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    fig.legend(legend_lines, labels, fontsize='small', loc='upper right', bbox_to_anchor=(0.9975, 0.96))
    plt.show()
