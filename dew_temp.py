import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
from matplotlib.widgets import Button, RangeSlider
import math
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition


def humidity_split(total_data):
    """
    This function takes the total data and splits it based on humidity percentage
    :param total_data: total data from all sensors
    :return: sorted_n_values: list containing the sorted humidity and the corresponding, temperature and dew point
             sorted_nw_values: list containing the sorted humidity and the corresponding, temperature and dew point
             sorted_se_values: list containing the sorted humidity and the corresponding, temperature and dew point
             sorted_sw_values: list containing the sorted humidity and the corresponding, temperature and dew point
    """
    n_values = []
    nw_values = []
    se_values = []
    sw_values = []
    for i in range(0, len(total_data[0][0]), 1):
        if not n_values:
            n_values.append([total_data[0][11][i]])
            n_values.append([total_data[0][4][i]])
            n_values.append([total_data[0][5][i]])
            nw_values.append([total_data[1][11][i]])
            nw_values.append([total_data[1][4][i]])
            nw_values.append([total_data[1][5][i]])
            se_values.append([total_data[2][11][i]])
            se_values.append([total_data[2][4][i]])
            se_values.append([total_data[2][5][i]])
            sw_values.append([total_data[3][11][i]])
            sw_values.append([total_data[3][4][i]])
            sw_values.append([total_data[3][5][i]])
        else:
            n_values[0].append(total_data[0][11][i])
            n_values[1].append(total_data[0][4][i])
            n_values[2].append(total_data[0][5][i])
            nw_values[0].append(total_data[1][11][i])
            nw_values[1].append(total_data[1][4][i])
            nw_values[2].append(total_data[1][5][i])
            se_values[0].append(total_data[2][11][i])
            se_values[1].append(total_data[2][4][i])
            se_values[2].append(total_data[2][5][i])
            sw_values[0].append(total_data[3][11][i])
            sw_values[1].append(total_data[3][4][i])
            sw_values[2].append(total_data[3][5][i])
    # sort both arrays
    sorted_n_values = [[], [], []]
    sorted_nw_values = [[], [], []]
    sorted_se_values = [[], [], []]
    sorted_sw_values = [[], [], []]
    sorted_n_values[0] = sorted(n_values[0])
    sorted_n_values[1] = [x for _, x in sorted(zip(n_values[0], n_values[1]))]
    sorted_n_values[2] = [x for _, x in sorted(zip(n_values[0], n_values[2]))]
    sorted_nw_values[0] = sorted(nw_values[0])
    sorted_nw_values[1] = [x for _, x in sorted(zip(nw_values[0], nw_values[1]))]
    sorted_nw_values[2] = [x for _, x in sorted(zip(nw_values[0], nw_values[2]))]
    sorted_se_values[0] = sorted(se_values[0])
    sorted_se_values[1] = [x for _, x in sorted(zip(se_values[0], se_values[1]))]
    sorted_se_values[2] = [x for _, x in sorted(zip(se_values[0], se_values[2]))]
    sorted_sw_values[0] = sorted(sw_values[0])
    sorted_sw_values[1] = [x for _, x in sorted(zip(sw_values[0], sw_values[1]))]
    sorted_sw_values[2] = [x for _, x in sorted(zip(sw_values[0], sw_values[2]))]
    #sorted_humidity_values = sorted(humidity_values)
    return sorted_n_values, sorted_nw_values, sorted_se_values, sorted_sw_values


def color_vector(humidities, colors):
    """
    Takes a list of humidities values and a vector of colors to return a list of the appropriate color based
    on range of humidities
    :param humidities: list of humidity vlaues
    :param colors: list of colors
    :return: n_colors: list of colors for markers at that humidity
    """
    colors_list = []
    for i in range(0, len(humidities), 1):
        if humidities[i] <= 10:
            colors_list.append(colors[0])
        elif 10 < humidities[i] <= 20:
            colors_list.append(colors[1])
        elif 20 < humidities[i] <= 30:
            colors_list.append(colors[2])
        elif 30 < humidities[i] <= 40:
            colors_list.append(colors[3])
        elif 40 < humidities[i] <= 50:
            colors_list.append(colors[4])
        elif 50 < humidities[i] <= 60:
            colors_list.append(colors[5])
        elif 60 < humidities[i] <= 70:
            colors_list.append(colors[6])
        elif 70 < humidities[i] <= 80:
            colors_list.append(colors[7])
        elif 80 < humidities[i] <= 90:
            colors_list.append(colors[8])
        else:
            colors_list.append(colors[9])
    return colors_list


def humidity_impact(laken_meas, lakenw_meas, lakese_meas, lakesw_meas, vector_color, gen_dir_types, sensor_colors):
    # Create colors for general directions
    total_data = [laken_meas, lakenw_meas, lakese_meas, lakesw_meas]
    n_values , nw_values, se_values, sw_values = humidity_split(total_data)
    # read map image and create figure with subplots
    img = plt.imread("map.png")
    fig = plt.figure(figsize=(10, 7))
    map_gs = fig.add_gridspec(1, 2, top=0.95, left=0.025, right=.85)
    map = fig.add_subplot(map_gs[0, 0])
    map.imshow(img)
    dew_temp = fig.add_subplot(map_gs[0, 1])
    # Specify sensor locations and labels
    x = [643, 558, 545, 314]
    y = [204, 296, 836, 712]
    labels = ['LakeN Sensor', 'LakeNW Sensor', 'LakeSW Sensor', 'LakeSE Sensor']
    # Create array of selected sensors
    selected_sensors = [False, False, False, False]
    # Function for when wind sensor button is clicked.
    def clicked(event, index):
        """Removes or adds makers to change shown sensor data
        event: button that was clicked on
        index: corresponding sensor index
        leg: figure legend object
        return: leg
        """
        # Change the status of the selected sensor
        selected_sensors[index] = not selected_sensors[index]
        # Change color of selected button
        if selected_sensors[index]:
            buttons[index].color = 'gray'
            buttons[index].label.set_color('black')
        else:
            buttons[index].color = 'white'
            buttons[index].label.set_color(sensor_colors[index])
        # Hide or show markers based on if the sensor has been selected.
        # Reset buttons to being unselected if all have been selected
        # Update the marker legend
        first_leg = fig.legends[0]
        sec_leg = fig.legends[1]
        first_leg.remove()
        sec_leg.remove()
        temp_leg_marks = legend_marks.copy()
        temp_leg_handles = labels.copy()
        # Get the current values
        hum_min_val, hum_max_val = hum_slider.val
        dew_min_val, dew_max_val = dew_slider.val
        temp_min_val, temp_max_val = temp_slider.val
        if all(selected_sensors) or not any(selected_sensors):
            # set the slider values to be the absolute min and max if the current value is a local min or max
            hum_slider.set_min(min_humidity)
            hum_slider.set_max(max_humidity)
            dew_slider.set_min(min_dew)
            dew_slider.set_max(max_dew)
            temp_slider.set_min(min_temp)
            temp_slider.set_max(max_temp)
            hum_slider.set_val((hum_min_val, hum_max_val))
            dew_slider.set_val((dew_min_val, dew_max_val))
            temp_slider.set_val((temp_min_val, temp_max_val))
            for i in range(0, 4, 1):
                buttons[i].color = 'white'
                buttons[i].label.set_color(sensor_colors[i])
                selected_sensors[i] = False
                scatters[i].set_visible(True)
        else:
            # Initialize the min and max slider values
            hum_min_temp = 100
            hum_max_temp = 0
            dew_min_temp = 100
            dew_max_temp = 0
            temp_min_temp = 100
            temp_max_temp = 0
            # Get current limits of plot
            temp_min_temp, temp_max_temp = dew_temp.get_ylim()
            for i in range(0, 4, 1):
                if not selected_sensors[i]:
                    scatters[i].set_visible(False)
                    temp_leg_marks.remove(legend_marks[i])
                    temp_leg_handles.remove(labels[i])
                else:
                    hum_min_temp = min(hum_min_temp, max(hum_min_val, hum_mins[i]))
                    hum_max_temp = max(hum_max_temp, min(hum_max_val, hum_maxs[i]))
                    dew_min_temp = min(dew_min_temp, max(dew_min_val, dew_mins[i]))
                    dew_max_temp = max(dew_max_temp, min(dew_max_val, dew_maxs[i]))
                    temp_min_temp = min(temp_min_temp, max(temp_min_val, temp_mins[i]))
                    temp_max_temp = max(temp_max_temp, min(temp_max_val, temp_maxs[i]))
                    scatters[i].set_visible(True)
            hum_slider.set_min(hum_min_temp)
            hum_slider.set_max(hum_max_temp)
            dew_slider.set_min(dew_min_temp)
            dew_slider.set_max(dew_max_temp)
            temp_slider.set_min(temp_min_temp)
            temp_slider.set_max(temp_max_temp)
            # Update slider values if needed
            hum_min_val = hum_min_temp if hum_min_temp > hum_min_val else hum_min_val
            hum_max_val = hum_max_temp if hum_max_temp > hum_max_val else hum_max_val
            dew_min_val = dew_min_temp if dew_min_temp > dew_min_val else dew_min_val
            dew_max_val = dew_max_temp if dew_max_temp > dew_max_val else dew_max_val
            temp_min_val = temp_min_temp if temp_min_temp > temp_min_val else temp_min_val
            temp_max_val = temp_max_temp if temp_max_temp > temp_max_val else temp_max_val
            # Set slider values
            hum_slider.set_val((hum_min_val, hum_max_val))
            dew_slider.set_val((dew_min_val, dew_max_val))
            temp_slider.set_val((temp_min_val, temp_max_val))
        fig.legend(temp_leg_marks, temp_leg_handles, fontsize=8, title='Sensors')
        fig.legend(legend_colors, legend_label, fontsize=8, loc='upper right', bbox_to_anchor=(0.9997, 0.86),
                   title='Humidity')
    # Plot sensors on the map and annotate with boxes
    map.scatter(x, y, color=sensor_colors)
    map.axis('off')

    # Create Sensor Filter buttons
    # Set position of the buttons
    n_loc = plt.axes([0, 0, 1, 1])
    n_ip = InsetPosition(map.axes, [0.565, 0.75, .29, 0.06])
    n_loc.set_axes_locator(n_ip)
    nw_loc = plt.axes([0, 0, 1, 1])
    nw_ip = InsetPosition(map.axes, [0.49, 0.665, .32, 0.06])
    nw_loc.set_axes_locator(nw_ip)
    se_loc = plt.axes([0, 0, 1, 1])
    se_ip = InsetPosition(map.axes, [0.48, 0.16, .32, 0.06])
    se_loc.set_axes_locator(se_ip)
    sw_loc = plt.axes([0, 0, 1, 1])
    sw_ip = InsetPosition(map.axes, [0.28, 0.28, .32, 0.06])
    sw_loc.set_axes_locator(sw_ip)

    # Create buttons, list of buttons and change font
    nbutton = Button(n_loc, 'LakeN Sensor', color='white')
    nwbutton = Button(nw_loc, 'LakeNW Sensor', color='white')
    sebutton = Button(se_loc, 'LakeSE Sensor', color='white')
    swbutton = Button(sw_loc, 'LakeSW Sensor', color='white')
    buttons = [nbutton, nwbutton, sebutton, swbutton]
    nbutton.label.set_color(sensor_colors[0])
    nbutton.label.set_horizontalalignment('center')
    nbutton.label.set_verticalalignment('center')
    nwbutton.label.set_color(sensor_colors[1])
    sebutton.label.set_color(sensor_colors[2])
    swbutton.label.set_color(sensor_colors[3])

    nbutton.on_clicked(lambda x: clicked(x, 0))
    nwbutton.on_clicked(lambda x: clicked(x, 1))
    sebutton.on_clicked(lambda x: clicked(x, 2))
    swbutton.on_clicked(lambda x: clicked(x, 3))

    # Create Slider
    min_humidity = min([n_values[0][0], nw_values[0][0], se_values[0][0], sw_values[0][0]])
    max_humidity = max([n_values[0][-1], nw_values[0][-1], se_values[0][-1], sw_values[0][-1]])
    hum_mins = [n_values[0][0], nw_values[0][0], se_values[0][0], sw_values[0][0]]
    hum_maxs = [n_values[0][-1], nw_values[0][-1], se_values[0][-1], sw_values[0][-1]]
    temp_mins = [min(n_values[1][:]), min(nw_values[1][:]), min(se_values[1][:]), min(sw_values[1][:])]
    temp_maxs = [max(n_values[1][:]), max(nw_values[1][:]), max(se_values[1][:]), max(sw_values[1][:])]
    dew_mins = [min(n_values[2][:]), min(nw_values[2][:]), min(se_values[2][:]), min(sw_values[2][:])]
    dew_maxs = [max(n_values[2][:]), max(nw_values[2][:]), max(se_values[2][:]), max(sw_values[2][:])]
    min_temp = math.floor(min([min(n_values[1][:]), min(nw_values[1][:]), min(se_values[1][:]), min(sw_values[1][:])]))
    max_temp = math.ceil(max([max(n_values[1][:]), max(nw_values[1][:]), max(se_values[1][:]), max(sw_values[1][:])]))
    min_dew = math.floor(min([min(n_values[2][:]), min(nw_values[2][:]), min(se_values[2][:]), min(sw_values[2][:])]))
    max_dew = math.ceil(max([max(n_values[2][:]), max(nw_values[2][:]), max(se_values[2][:]), max(sw_values[2][:])]))
    # Create sliders
    hum_slider = RangeSlider(plt.axes([0.12, 0.1, 0.24, 0.03]), '% Humidity ', min_humidity, max_humidity,
                             valinit=(min_humidity, max_humidity), valfmt='%-G', valstep=1)
    dew_slider = RangeSlider(plt.axes([0.12, 0.15, 0.24, 0.03]), 'Dew Point (\xb0F) ', min_dew, max_dew,
                             valinit=(min_dew, max_dew), valfmt='%-G', valstep=1)
    temp_slider = RangeSlider(plt.axes([0.12, 0.2, 0.24, 0.03]), 'Temp. (\xb0F) ', min_temp, max_temp,
                             valinit=(min_temp, max_temp), valfmt='%-G', valstep=1)

    def humchanged(min_max):
        """
        changes the offset of the scatter plots based on the humidity slider to remove or add markers
        :param min_max: Slider max and min values
        """
        # Change the LakeN Sensor
        if min_max[0] in n_values[0]:
            lower = n_values[0].index(min_max[0])
        else:
            lower = 0
        temp = n_values[0].copy()
        temp.reverse()
        if min_max[1] in n_values[0]:
            upper = len(n_values[0]) - temp.index(min_max[1])
        else:
            upper = len(n_values[0])
        scatters[0].set_offsets(np.stack((n_values[1][lower:upper], n_values[2][lower:upper]), axis=1))
        colors_update = color_vector(n_values[0][lower:upper], plot_colors)
        scatters[0].set_color(colors_update)
        # Change the LakeNW Sensor
        if min_max[0] in nw_values[0]:
            lower = nw_values[0].index(min_max[0])
        else:
            lower = 0
        temp = nw_values[0].copy()
        temp.reverse()
        if min_max[1] in nw_values[0]:
            upper = len(nw_values[0]) - temp.index(min_max[1])
        else:
            upper = len(nw_values[0])
        scatters[1].set_offsets(np.stack((nw_values[1][lower:upper], nw_values[2][lower:upper]), axis=1))
        colors_update = color_vector(nw_values[0][lower:upper], plot_colors)
        scatters[1].set_color(colors_update)
        # Change the LakeSE Sensor
        if min_max[0] in se_values[0]:
            lower = se_values[0].index(min_max[0])
        else:
            lower = 0
        temp = se_values[0].copy()
        temp.reverse()
        if min_max[1] in se_values[0]:
            upper = len(se_values[0]) - temp.index(min_max[1])
        else:
            upper = len(se_values[0])
        scatters[2].set_offsets(np.stack((se_values[1][lower:upper], se_values[2][lower:upper]), axis=1))
        colors_update = color_vector(se_values[0][lower:upper], plot_colors)
        scatters[2].set_color(colors_update)
        # Change the LakeSW Sensor
        if min_max[0] in sw_values[0]:
            lower = sw_values[0].index(min_max[0])
        else:
            lower = 0
        temp = sw_values[0].copy()
        temp.reverse()
        if min_max[1] in sw_values[0]:
            upper = len(sw_values[0]) - temp.index(min_max[1])
        else:
            upper = len(sw_values[0])
        scatters[3].set_offsets(np.stack((sw_values[1][lower:upper], sw_values[2][lower:upper]), axis=1))
        colors_update = color_vector(sw_values[0][lower:upper], plot_colors)
        scatters[3].set_color(colors_update)

    def dewchanged(min_max):
        """
        Changes the scatter plot to eliminate plots above and below the dew slider values
        :param min_max: Slider max and min values
        """
        dew_temp.set_ylim(min_max)

    def tempchange(min_max):
        """
        Changes the scatter plot to eliminate plots above and below the dew slider values
        :param min_max: Slider max and min values
        """
        dew_temp.set_xlim(min_max)

    hum_slider.on_changed(humchanged)
    dew_slider.on_changed(dewchanged)
    temp_slider.on_changed(tempchange)


    # Loop through the four sensors to plot the data
    scatters = []
    marker_types = ['+', '.', '1', '*']
    plot_colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray',
                   'tab:olive', 'tab:cyan']
    legend_label = ['0 - 10 %', '10 - 20 %', '20 - 30 %', '30 - 40 %', '40 - 50 %', '50 - 60 %',
                    '60 - 70 %', '70 - 80 %', '80 - 90 %', '90 - 100 %']
    # loop through the sensor humidities and determine the humidity range
    n_colors = color_vector(n_values[0], plot_colors)
    nw_colors = color_vector(nw_values[0], plot_colors)
    se_colors = color_vector(se_values[0], plot_colors)
    sw_colors = color_vector(sw_values[0], plot_colors)

    # Plot Dew Point vs temperature.
    scatters.append(dew_temp.scatter(n_values[1], n_values[2], marker=marker_types[0],
                                      color=n_colors))
    scatters.append(dew_temp.scatter(nw_values[1], nw_values[2], marker=marker_types[1],
                                       color=nw_colors))
    scatters.append(dew_temp.scatter(se_values[1], se_values[2], marker=marker_types[2],
                                       color=se_colors))
    scatters.append(dew_temp.scatter(sw_values[1], sw_values[2], marker=marker_types[3],
                                       color=sw_colors))

    # Create lines to make custom legend
    legend_marks = []
    labels = ['LakeN', 'LakeNW', 'LakeSW', 'LakeSE']
    for i in range(0, len(marker_types), 1):
        legend_marks.append(Line2D([0], [0], marker=marker_types[i], markeredgecolor='black', color='white'))
    legend_colors = []
    for i in range(0, len(plot_colors), 1):
       legend_colors.append(Patch(color=plot_colors[i], label=legend_label[i]))
    # Set the titles and axis labels of the plots
    dew_temp.set_title('Temperature vs Dew Point')
    dew_temp.set_xlabel('$Temperature (^oF)$')
    dew_temp.set_ylabel('$Dew Point (^oF)$')
    # Modify the plot axis and tick marks
    plt.draw()
    dew_temp.set_xticks(dew_temp.get_xticks())
    fig.legend(legend_marks, labels, fontsize=8, title='Sensors')
    fig.legend(legend_colors, legend_label, fontsize=8, loc='upper right', bbox_to_anchor=(0.9997, 0.86),
               title='Humidity')
    plt.show()
