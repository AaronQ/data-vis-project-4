from wind_vector import plot_quiver
from rad_temp import shade_temp
from dew_temp import humidity_impact


def read_file(file_name):
  """
  reads in the file and puts the data into lists
  :param file_name: name of the input file
  :return: laken_meas: list of lists containing list of measurement values from LakeN sensor
           lakenw_meas: list of lists containing list of measurement values from LakeNW sensor
           lakese_meas: list of lists containing list of measurement values from LakeSE sensor
           lakesw_meas: list of lists containing list of measurement values from LakeSW sensor
  """
  with open(file_name, 'r') as f:
    variables = f.readline().rstrip().split(',')
    laken_meas = []
    lakenw_meas = []
    lakese_meas = []
    lakesw_meas = []
    for line in f:
      line_txt = line.rstrip().split(',')
      if line_txt[1] == 'LakeN':
        laken_meas.append(line_txt)
      elif line_txt[1] == 'LakeNW':
        lakenw_meas.append(line_txt)
      elif line_txt[1] == 'LakeSE':
        lakese_meas.append(line_txt)
      elif line_txt[1] == 'LakeSW':
        lakesw_meas.append(line_txt)
  # Transpose the data
  laken_meas = list(map(list, zip(*laken_meas)))
  lakenw_meas = list(map(list, zip(*lakenw_meas)))
  lakese_meas = list(map(list, zip(*lakese_meas)))
  lakesw_meas = list(map(list, zip(*lakesw_meas)))
  # Convert values of measurements to numbers for North Sensor
  laken_meas[2] = [float(x) for x in laken_meas[2]]
  laken_meas[3] = [float(x) for x in laken_meas[3]]
  laken_meas[4] = [float(x) for x in laken_meas[4]]
  laken_meas[5] = [float(x) for x in laken_meas[5]]
  laken_meas[6] = [float(x) for x in laken_meas[6]]
  laken_meas[7] = [float(x) for x in laken_meas[7]]
  laken_meas[8] = [float(x) for x in laken_meas[8]]
  laken_meas[10] = [float(x) for x in laken_meas[10]]
  laken_meas[11] = [float(x) for x in laken_meas[11]]
  laken_meas[12] = [float(x) for x in laken_meas[12]]
  laken_meas[13] = [float(x) for x in laken_meas[13]]
  # Convert values of measurements to numbers for Northwest Sensor
  lakenw_meas[2] = [float(x) for x in lakenw_meas[2]]
  lakenw_meas[3] = [float(x) for x in lakenw_meas[3]]
  lakenw_meas[4] = [float(x) for x in lakenw_meas[4]]
  lakenw_meas[5] = [float(x) for x in lakenw_meas[5]]
  lakenw_meas[6] = [float(x) for x in lakenw_meas[6]]
  lakenw_meas[7] = [float(x) for x in lakenw_meas[7]]
  lakenw_meas[8] = [float(x) for x in lakenw_meas[8]]
  lakenw_meas[10] = [float(x) for x in lakenw_meas[10]]
  lakenw_meas[11] = [float(x) for x in lakenw_meas[11]]
  lakenw_meas[12] = [float(x) for x in lakenw_meas[12]]
  lakenw_meas[13] = [float(x) for x in lakenw_meas[13]]
  # Convert values of measurements to numbers for Southeast Sensor
  lakese_meas[2] = [float(x) for x in lakese_meas[2]]
  lakese_meas[3] = [float(x) for x in lakese_meas[3]]
  lakese_meas[4] = [float(x) for x in lakese_meas[4]]
  lakese_meas[5] = [float(x) for x in lakese_meas[5]]
  lakese_meas[6] = [float(x) for x in lakese_meas[6]]
  lakese_meas[7] = [float(x) for x in lakese_meas[7]]
  lakese_meas[8] = [float(x) for x in lakese_meas[8]]
  lakese_meas[10] = [float(x) for x in lakese_meas[10]]
  lakese_meas[11] = [float(x) for x in lakese_meas[11]]
  lakese_meas[12] = [float(x) for x in lakese_meas[12]]
  lakese_meas[13] = [float(x) for x in lakese_meas[13]]
  # Convert values of measurements to numbers for Southwest Sensor
  lakesw_meas[2] = [float(x) for x in lakesw_meas[2]]
  lakesw_meas[3] = [float(x) for x in lakesw_meas[3]]
  lakesw_meas[4] = [float(x) for x in lakesw_meas[4]]
  lakesw_meas[5] = [float(x) for x in lakesw_meas[5]]
  lakesw_meas[6] = [float(x) for x in lakesw_meas[6]]
  lakesw_meas[7] = [float(x) for x in lakesw_meas[7]]
  lakesw_meas[8] = [float(x) for x in lakesw_meas[8]]
  lakesw_meas[10] = [float(x) for x in lakesw_meas[10]]
  lakesw_meas[11] = [float(x) for x in lakesw_meas[11]]
  lakesw_meas[12] = [float(x) for x in lakesw_meas[12]]
  lakesw_meas[13] = [float(x) for x in lakesw_meas[13]]
  return laken_meas, lakenw_meas, lakese_meas, lakesw_meas


def sort_readings(laken_meas, lakenw_meas, lakese_meas, lakesw_meas):
  """
  takes sensor measurements, sorts based on datetime, then returns the sorted measurements
  :param laken_meas:
  :param lakenw_meas:
  :param lakese_meas:
  :param lakesw_meas:
  :return: sorted_laken, sorted_lakenw, sorted_lakese, sorted_lakesw
  """
  sorted_laken = [[], [], [], [], [], [], [], [], [], [], [], [], [], []]
  sorted_lakenw = [[], [], [], [], [], [], [], [], [], [], [], [], [], []]
  sorted_lakese = [[], [], [], [], [], [], [], [], [], [], [], [], [], []]
  sorted_lakesw = [[], [], [], [], [], [], [], [], [], [], [], [], [], []]

  sorted_laken[0] = sorted(laken_meas[0])
  sorted_laken[1] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[1]))]
  sorted_laken[2] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[2]))]
  sorted_laken[3] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[3]))]
  sorted_laken[4] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[4]))]
  sorted_laken[5] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[5]))]
  sorted_laken[6] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[6]))]
  sorted_laken[7] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[7]))]
  sorted_laken[8] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[8]))]
  sorted_laken[9] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[9]))]
  sorted_laken[10] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[10]))]
  sorted_laken[11] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[11]))]
  sorted_laken[12] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[12]))]
  sorted_laken[13] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[13]))]

  sorted_lakenw[0] = sorted(lakenw_meas[0])
  sorted_lakenw[1] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[1]))]
  sorted_lakenw[2] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[2]))]
  sorted_lakenw[3] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[3]))]
  sorted_lakenw[4] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[4]))]
  sorted_lakenw[5] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[5]))]
  sorted_lakenw[6] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[6]))]
  sorted_lakenw[7] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[7]))]
  sorted_lakenw[8] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[8]))]
  sorted_lakenw[9] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[9]))]
  sorted_lakenw[10] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[10]))]
  sorted_lakenw[11] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[11]))]
  sorted_lakenw[12] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[12]))]
  sorted_lakenw[13] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[13]))]

  sorted_lakese[0] = sorted(lakese_meas[0])
  sorted_lakese[1] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[1]))]
  sorted_lakese[2] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[2]))]
  sorted_lakese[3] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[3]))]
  sorted_lakese[4] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[4]))]
  sorted_lakese[5] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[5]))]
  sorted_lakese[6] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[6]))]
  sorted_lakese[7] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[7]))]
  sorted_lakese[8] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[8]))]
  sorted_lakese[9] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[9]))]
  sorted_lakese[10] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[10]))]
  sorted_lakese[11] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[11]))]
  sorted_lakese[12] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[12]))]
  sorted_lakese[13] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[13]))]


  sorted_lakesw[0] = sorted(lakesw_meas[0])
  sorted_lakesw[1] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[1]))]
  sorted_lakesw[2] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[2]))]
  sorted_lakesw[3] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[3]))]
  sorted_lakesw[4] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[4]))]
  sorted_lakesw[5] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[5]))]
  sorted_lakesw[6] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[6]))]
  sorted_lakesw[7] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[7]))]
  sorted_lakesw[8] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[8]))]
  sorted_lakesw[9] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[9]))]
  sorted_lakesw[10] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[10]))]
  sorted_lakesw[11] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[11]))]
  sorted_lakesw[12] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[12]))]
  sorted_lakesw[13] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[13]))]

  return sorted_laken, sorted_lakenw, sorted_lakese, sorted_lakesw


if __name__ == '__main__':
    # read data from file
    laken_meas, lakenw_meas, lakese_meas, lakesw_meas = read_file('output.csv')

    sorted_laken, sorted_lakenw, sorted_lakese, sorted_lakesw = sort_readings(laken_meas, lakenw_meas,
                                                                              lakese_meas, lakesw_meas)

    vector_color = ['#084594', '#ae1082',  '#0082a3', '#feb24c', '#f37625', '#4daf4a',
                    '#a7b43d', '#e41a1c']
    gen_dir_types = ['North', 'Northeast', 'East', 'Southeast', 'South', 'Southwest', 'West', 'Northwest']
    sensor_colors = ['#084594', '#e41a1c', '#feb24c', '#4daf4a']
    plot_quiver(sorted_laken, sorted_lakenw, sorted_lakese, sorted_lakesw, vector_color, gen_dir_types, sensor_colors)
    shade_temp(sorted_laken, sorted_lakenw, sorted_lakese, sorted_lakesw, vector_color, gen_dir_types, sensor_colors)
    humidity_impact(sorted_laken, sorted_lakenw, sorted_lakese, sorted_lakesw, vector_color, gen_dir_types, sensor_colors)
